import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicUsersComponent } from './technic-users.component';

describe('TechnicUsersComponent', () => {
  let component: TechnicUsersComponent;
  let fixture: ComponentFixture<TechnicUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnicUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

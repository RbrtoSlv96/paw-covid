import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { RestService } from '../rest.service';
import { result } from '../model/result';

@Component({
  selector: 'app-technic-users',
  templateUrl: './technic-users.component.html',
  styleUrls: ['./technic-users.component.css']
})
export class TechnicUsersComponent implements OnInit {

  user: User
  mode = 'list';


  constructor(public restService: RestService, public route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((paramMap: ParamMap) =>{
      if (paramMap.has('id')){
        this.mode = 'edit'
      } else {
        this.mode = 'list'
      }
    })
  }

}

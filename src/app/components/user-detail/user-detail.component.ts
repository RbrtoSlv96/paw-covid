import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../model/user';
import { sex } from '../model/sex';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  user: User;
  users: Array<User> = []
  dataSource = new MatTableDataSource()
  displayedColumns: string[] = ['name', 'nif', 'age', 'email', 'state', 'edit'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(public rest: RestService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.getAllUsers();
    this.dataSource.paginator = this.paginator
  }

  getUserById(id: String) {
    this.rest.getUser(id).subscribe((data: User) => {
      this.user = data;
      this.users.push(this.user);
    })
  }

  getUserByNif(nif: Number) {
    this.rest.getUserNif(nif).subscribe((data: User) => {
      this.user = data;
      this.users.push(this.user);
    })
  }

  getAllUsers() {
    this.rest.getAllUsers().subscribe((data: Array<User>) => {
      this.users = data;
      this.dataSource.data = this.users
    })
  }

  filterTable(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}



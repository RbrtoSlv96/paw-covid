import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicHomeComponent } from './technic-home.component';

describe('TechnicHomeComponent', () => {
  let component: TechnicHomeComponent;
  let fixture: ComponentFixture<TechnicHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnicHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

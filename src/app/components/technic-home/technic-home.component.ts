import { Component, OnInit } from '@angular/core';
import { Technic } from '../model/technic';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-technic-home',
  templateUrl: './technic-home.component.html',
  styleUrls: ['./technic-home.component.css']
})
export class TechnicHomeComponent implements OnInit {
  technicCurrent: number
  technic: Technic;

  constructor(public rest: RestService ) { }

  ngOnInit(): void {
    this.technicCurrent = parseInt(localStorage.getItem('technicNIF'), 10)
    this.rest.getOneTechnicByNif(this.technicCurrent).subscribe((data: Technic) =>{
      this.technic = data;
      console.log(this.technic)
    })
  }

}

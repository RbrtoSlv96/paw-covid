import { Component, OnInit, Input } from '@angular/core';
import { User } from '../model/user';
import { Validators, FormControl, NgForm } from '@angular/forms';
import { RestService } from '../rest.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: User
  id: string
  data: User
  constructor(public rest: RestService, public route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('id')) {
        this.id = paramMap.get('id');
        this.rest.getUser(this.id).subscribe((data: User) => {
          this.user = data;   
        })
      }
    })
  }



  updateUser() {
    this.rest.UpdateUserById(this.user, this.user._id).subscribe(data => { alert("Succesfully Update user") }, Error => { alert("failed while updating user") });
    this.router.navigate(['/technic/users']);
  }



}

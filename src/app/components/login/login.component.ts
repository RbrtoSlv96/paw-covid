import { Component, OnInit, } from '@angular/core';
import { Router } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthenticationService } from '../AuthenticationService';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  username: number;
  password: string;

  errors: number;


  constructor(private router: Router, private authService: AuthenticationService) { }

  ngOnInit(): void {
  }



  login(event, form: NgForm): void {
    event.preventDefault()
    this.authService.login(this.username, this.password)
      .subscribe(
        (technic: any) => {
          localStorage.setItem('currentUser', JSON.stringify(technic));
          localStorage.setItem('technicNIF', this.username.toString())
          this.router.navigate(['/technic/home', this.username])
        },
        (error) => {
          if (error.status != null) {
            this.errors = error.status
            form.reset();
          }
        }
      )
  }


}

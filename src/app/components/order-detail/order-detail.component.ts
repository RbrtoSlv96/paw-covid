import { Component, OnInit, ViewChild } from '@angular/core';
import { Order } from '../model/order';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css']
})
export class OrderDetailComponent implements OnInit {
  order: Order;
  orders: Array<Order> = [];
  dataSource = new MatTableDataSource()
  displayedColumns: string[] = ['userId','state', 'result', 'edit'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(public rest: RestService, private route: ActivatedRoute, private router: Router) {

  }

  ngOnInit(): void {
    this.getAllOrders();
    this.dataSource.paginator = this.paginator
  }

  getOneOrderByUserId(id: String) {
    this.rest.getOrderByUserId(id).subscribe((data: Order) => {
      this.order = data;
      this.orders.push(this.order);
    })
  }

  getOneOrder(id: String) {
    this.rest.getOneOrder(id).subscribe((data: Order) => {
      this.order = data;
      this.orders.push(this.order);
    })
  }

  getAllOrders() {
    this.rest.getAllOrders().subscribe((data: Array<Order>) => {
      this.orders = data;
      this.dataSource.data = this.orders;
    })
  }

  filterTable(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}

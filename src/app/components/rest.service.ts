import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { User } from './model/user';
import { Technic } from './model/technic';
import { Order } from './model/order';

const endpoint = 'https://rest-api-covid.herokuapp.com/api/v1';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class RestService {

  constructor(private http: HttpClient) { }

  /////////////////////
  // API REST Users //
  ////////////////////

  //Método que retorna erro da API REST
  private handleError(error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }

  //Obtém todos os users da API REST
  getAllUsers(): Observable<Array<User>> {
    return this.http.get<Array<User>>(endpoint + '/users')
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Adiciona um user na API REST
  CreateUser(user: User): Observable<User> {
    return this.http.post<User>(endpoint + '/users', JSON.stringify(user), httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Obtém todos os users infetados
  getAllUsersInfect(): Observable<Array<User>> {
    return this.http.get<Array<User>>(endpoint + '/users/infecteds')
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Obtém o número de infetados
  getNumberUsersInfect(): Observable<Number> {
    return this.http.get<Number>(endpoint + '/users/infecteds/number')
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Obtém o user através do ID
  getUser(id: String): Observable<User> {
    return this.http.get<User>(endpoint + '/user/id/' + id)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Dá update a um user já existente na API REST
  UpdateUserById(user: User, id: String): Observable<User> {
    return this.http.put<User>(endpoint + '/user/id/' + id, JSON.stringify(user), httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Remove o user da API REST  através do ID
  DeleteUserById(user: User): Observable<User> {
    return this.http.delete<User>(endpoint + '/user/id/' + user._id, httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Obtém o user através do NIF
  getUserNif(nif: Number): Observable<User> {
    return this.http.get<User>(endpoint + '/user/nif/' + nif)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Dá update a um user já existente através do NIF na API REST
  UpdateUserByNif(user: User): Observable<User> {
    return this.http.put<User>(endpoint + '/user/nif/' + user.nif, JSON.stringify(user), httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Remove o user da API REST  através do NIF
  DeleteUserByNif(user: User): Observable<User> {
    return this.http.delete<User>(endpoint + '/user/nif/' + user.nif, httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  ////////////////////////
  // API REST Técnicos //
  ///////////////////////

  //Obtém todos os técnicos
  getAllTechnics(): Observable<Array<Technic>> {
    return this.http.get<Array<Technic>>(endpoint + '/technics')
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Obtém um técnico através do ID
  getOneTechnicById(id: String): Observable<Technic> {
    return this.http.get<Technic>(endpoint + '/technic/id/' + id)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Adiciona uma técnico na API REST
  CreateTechnic(technic: Technic): Observable<Technic> {
    return this.http.post<Technic>(endpoint + '/technics', JSON.stringify(technic), httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Dá update a um técnico na API REST
  UpdateTechnicById(technic: Technic): Observable<Technic> {
    return this.http.put<Technic>(endpoint + '/technic/id/' + technic._id, JSON.stringify(technic), httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Remove um técnico da API REST
  DeleteTechnicById(technic: Technic): Observable<Technic> {
    return this.http.delete<Technic>(endpoint + '/technic/id/' + technic._id, httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Obtém um técnico através do NIF
  getOneTechnicByNif(nif: Number): Observable<Technic> {
    return this.http.get<Technic>(endpoint + '/technic/nif/' + nif)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Dá update a um técnico na API REST
  UpdateTechnicByNif(technic: Technic): Observable<Technic> {
    return this.http.put<Technic>(endpoint + '/technic/nif/' + technic.nif, JSON.stringify(technic), httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Remove um técnico da API REST
  DeleteTechnicByNif(technic: Technic): Observable<Technic> {
    return this.http.delete<Technic>(endpoint + '/technic/nif/' + technic.nif, httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  ///////////////////////
  // API REST Pedidos //
  //////////////////////

  //Otém todos os pedidos da API REST
  getAllOrders(): Observable<Array<Order>> {
    return this.http.get<Array<Order>>(endpoint + '/orders')
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Obtém um pedido através do ID do user
  getOrderByUserId(userid: String): Observable<Order> {
    return this.http.get<Order>(endpoint + '/orders/' + userid)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Adiciona um pedido através do ID do user
  CreateOrderByUserId(order: Order, userid: String): Observable<Order> {
    return this.http.post<Order>(endpoint + '/orders/' + userid, JSON.stringify(order), httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Obtém um pedido através do ID
  getOneOrder(id: String): Observable<Order> {
    return this.http.get<Order>(endpoint + '/order/' + id)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Dá update a um pedido na API REST
  UpdateOrderById(order: Order, orderId : String): Observable<Order> {
    return this.http.put<Order>(endpoint + '/order/' + orderId, JSON.stringify(order), httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  //Remove um pedido na API REST
  DeleteOrderById(order: Order): Observable<Order> {
    return this.http.delete<Order>(endpoint + '/order/' + order._id, httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }
}

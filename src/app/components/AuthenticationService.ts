import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, ObservableInput } from 'rxjs';

const endpoint = 'https://rest-api-covid.herokuapp.com/api/v1/auth';
const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable({
    providedIn: 'root'
})

export class AuthenticationService {

    constructor(private http: HttpClient) { }

    login(nif: number, password: string): Observable<any> {
        return this.http.post<any>(endpoint + '/login', { nif, password });
    }

    logout(){
        localStorage.removeItem('currentUser');
    }


}

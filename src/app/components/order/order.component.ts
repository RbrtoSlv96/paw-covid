import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { User } from '../model/user';
import { Order } from '../model/order';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { events } from '../model/events';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  user: User;
  order: Order;
  _id: string
  email = new FormControl('', [Validators.required, Validators.email]);
  healthState = new FormControl();
  events: events[];
  sex = new FormControl();
  riscGroup = new FormControl(false);
  nif = new FormControl('', [Validators.required, Validators.max(999999999), Validators.min(100000000)]);
  contact = new FormControl('', [Validators.required, Validators.max(999999999), Validators.min(100000000)]);
  selected = 'yes';
  callHealth24 = new FormControl(false);
  riscWorkPlace = new FormControl(false);
  option: boolean;
  fullname: string;
  address: string;
  age: number;
  city: string;



  constructor(public rest: RestService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.option = true;
  }

  submit(): void {
    this.user = new User(this.fullname, this.age, this.email.value, this.sex.value, this.nif.value, this.contact.value, this.address, this.city);
    this.rest.CreateUser(this.user).subscribe(data => { alert("Succesfully Added Users") }, Error => { alert("failed while adding user") });
    this.rest.getUserNif(this.user.nif).subscribe(data => {
      if (this.riscGroup.value && this.riscWorkPlace.value && this.callHealth24.value) {
        this.events = [events.riscGroup, events.callHealth24, events.riscWorkPlace];
      } else if (this.riscGroup.value && this.riscWorkPlace.value) {
        this.events = [events.riscGroup, events.riscWorkPlace];
      } else if (this.riscGroup.value && this.callHealth24.value) {
        this.events = [events.riscGroup, events.callHealth24];
      } else if (this.riscWorkPlace.value && this.callHealth24.value) {
        this.events = [events.riscWorkPlace, events.callHealth24];
      } else if (this.riscGroup.value) {
        this.events = [events.riscGroup];
      } else if (this.riscWorkPlace.value) {
        this.events = [events.riscWorkPlace];
      } else if (this.callHealth24.value) {
        this.events = [events.callHealth24];
      }
      this.order = new Order(data._id, this.healthState.value, this.events);
      this.rest.CreateOrderByUserId(this.order, data._id).subscribe(data => { alert("Succesfully Added Order") }, Error => { alert("failed while adding order") });
      this.router.navigate(['']);
    })


  }

  submitInUser(): void {
    this.rest.getUserNif(this.nif.value).subscribe(data => {
      if (data == null) {
        { alert("User not exist") };
      } else {
        if (this.riscGroup.value && this.riscWorkPlace.value && this.callHealth24.value) {
          this.events = [events.riscGroup, events.callHealth24, events.riscWorkPlace];
        } else if (this.riscGroup.value && this.riscWorkPlace.value) {
          this.events = [events.riscGroup, events.riscWorkPlace];
        } else if (this.riscGroup.value && this.callHealth24.value) {
          this.events = [events.riscGroup, events.callHealth24];
        } else if (this.riscWorkPlace.value && this.callHealth24.value) {
          this.events = [events.riscWorkPlace, events.callHealth24];
        } else if (this.riscGroup.value) {
          this.events = [events.riscGroup];
        } else if (this.riscWorkPlace.value) {
          this.events = [events.riscWorkPlace];
        } else if (this.callHealth24.value) {
          this.events = [events.callHealth24];
        }
        this.order = new Order(data._id, this.healthState.value, this.events);
        this.rest.CreateOrderByUserId(this.order, data._id).subscribe(data => { alert("Succesfully Added Order") }, Error => { alert("failed while adding order") });
        this.router.navigate(['']);
      }
    });


  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'Necessita de introduzir um valor';
    }

    return this.email.hasError('email') ? 'Email inválido' : '';
  }

  getErrorMessageNif() {
    if (this.nif.hasError('required')) {
      return 'Necessita de introduzir um valor';
    }

    if (this.nif.hasError('max')) {
      return 'NIF inválido';
    }

    return this.nif.hasError('min') ? 'NIF inválido' : '';
  }

  getErrorMessageContact() {
    if (this.contact.hasError('required')) {
      return 'Necessita de introduzir um valor';
    }

    if (this.contact.hasError('max')) {
      return 'Contacto inválido';
    }

    return this.contact.hasError('min') ? 'Contacto inválido' : '';
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { NgModule } from '@angular/core';
import { RestService } from 'src/app/components/rest.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../AuthenticationService';
import { Router } from '@angular/router';
import { User } from '../model/user';

@Component({
  selector: 'app-search-user',
  templateUrl: './search-user.component.html',
  styleUrls: ['./search-user.component.css']
})
export class SearchUserComponent implements OnInit {

  nif: number
  user: User

  constructor(private router: Router, public users: RestService, private authService: AuthenticationService, public route: ActivatedRoute) { }

  ngOnInit(): void {
  }

  onSearch(nif: number){
    this.users
    .getUserNif(nif)
    .subscribe((result) => {
      this.user = result
    })
  }

}

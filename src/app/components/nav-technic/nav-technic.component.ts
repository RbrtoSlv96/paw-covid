import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../AuthenticationService';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-nav-technic',
  templateUrl: './nav-technic.component.html',
  styleUrls: ['./nav-technic.component.css']
})
export class NavTechnicComponent implements OnInit {
  technicNIF: number
  constructor(private route: ActivatedRoute, private authService: AuthenticationService) { }

  ngOnInit(): void {
    this.technicNIF = parseInt(localStorage.getItem('technicNIF'), 10)
  }

  logout(): void {
    this.authService.logout();
    localStorage.removeItem('technicNIF');
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavTechnicComponent } from './nav-technic.component';

describe('NavTechnicComponent', () => {
  let component: NavTechnicComponent;
  let fixture: ComponentFixture<NavTechnicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavTechnicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavTechnicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

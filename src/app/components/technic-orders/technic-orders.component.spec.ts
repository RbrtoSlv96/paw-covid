import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicOrdersComponent } from './technic-orders.component';

describe('TechnicOrdersComponent', () => {
  let component: TechnicOrdersComponent;
  let fixture: ComponentFixture<TechnicOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnicOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

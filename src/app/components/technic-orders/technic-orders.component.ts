import { Component, OnInit } from '@angular/core';
import { Order } from '../model/order';
import { RestService } from '../rest.service';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-technic-orders',
  templateUrl: './technic-orders.component.html',
  styleUrls: ['./technic-orders.component.css']
})
export class TechnicOrdersComponent implements OnInit {
  order: Order
  mode = 'list';


  constructor(public restService: RestService, public route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((paramMap: ParamMap) =>{
      if (paramMap.has('id')){
        this.mode = 'edit'
      } else {
        this.mode = 'list'
      }
    })
  }


}

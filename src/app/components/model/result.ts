export enum result {
    waiting = "waiting",
    positive = "positive",
    negative = "negative"
}

import { sex } from '../model/sex';
import { state } from './state';

export class User {
    _id: string;
    fullname: string;
    age: number;
    email: string;
    sex: sex;
    nif: number;
    phone: number;
    state: state;
    address: string;
    city: string;


    constructor(fullname: string, age: number, email: string, sex: sex, nif: number, phone: number, address: string, city: string) {
        this.fullname = fullname;
        this.age = age;
        this.email = email;
        this.sex = sex;
        this.nif = nif;
        this.phone = phone;
        this.address = address;
        this.city = city;

    }

}

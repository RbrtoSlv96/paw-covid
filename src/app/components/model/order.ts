import { result } from '../model/result';
import { state } from '../model/state';
import { events } from '../model/events';
import * as moment from 'moment';


export class Order {
    _id: String;
    userId: String;
    date: Date;
    healthState: String;
    events: events[];
    exameDate: Date;
    state: state;
    result: result;

    constructor(userId: String, healthState: String, events: events[]) {
        this.userId = userId;
        this.date = new Date(moment().format('DD/MM/YYYY'));
        this.healthState = healthState;
        this.events = events;
        this.state = state.waiting;
        this.result = result.waiting;
    }
}
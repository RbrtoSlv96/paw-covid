import { sex } from '../model/sex';

export class Technic {
    _id: string;
    fullname: string;
    age: number;
    email: string;
    sex: sex;
    nif: number;
    phone: number;
    address: string;
    city: string;
    password:string;

    constructor(fullname: string, age: number, email: string, sex: sex, nif: number, phone: number, address: string, city: string, password:string) {
        this.fullname = fullname;
        this.age = age;
        this.email = email;
        this.sex = sex;
        this.nif = nif;
        this.phone = phone;
        this.address = address;
        this.city = city;
        this.password = password;
    }
}

import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Order } from '../model/order';

@Component({
  selector: 'app-order-edit',
  templateUrl: './order-edit.component.html',
  styleUrls: ['./order-edit.component.css']
})
export class OrderEditComponent implements OnInit {
  order: Order
  id: string
  data: Order

  constructor(public rest: RestService, public route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('id')) {
        this.id = paramMap.get('id');
        this.rest.getOneOrder(this.id).subscribe((data: Order) => {
          this.order = data;   
        })
      }
    })
  }

  updateOrder() {
    this.rest.UpdateOrderById(this.order, this.order._id).subscribe(data => { alert("Succesfully Update order") }, Error => { alert("failed while updating order") });
    this.router.navigate(['/technic/orders']);
  }
}

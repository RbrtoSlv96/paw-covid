import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserDetailComponent } from '../app/components/user-detail/user-detail.component';
import { OrderDetailComponent } from '../app/components/order-detail/order-detail.component';
import { NavComponent } from '../app/components/nav/nav.component';
import { HomepageComponent } from '../app/components/homepage/homepage.component';
import { LoginComponent } from '../app/components/login/login.component';
import { ContactsComponent } from '../app/components/contacts/contacts.component';
import { NavTechnicComponent } from '../app/components/nav-technic/nav-technic.component';
import { AuthGuard } from './components/AuthGuard';
import { AuthenticationService } from './components/AuthenticationService';
import { SearchUserComponent } from './components/search-user/search-user.component';
import { TechnicUsersComponent } from '../app/components/technic-users/technic-users.component';
import { TechnicHomeComponent } from '../app/components/technic-home/technic-home.component';
import { OrderComponent } from './components/order/order.component';
import { TechnicOrdersComponent } from './components/technic-orders/technic-orders.component';

const routes: Routes = [
  { path: 'user-detail', component: UserDetailComponent},
  { path: 'order-detail', component: OrderDetailComponent },
  { path: '', component: HomepageComponent },
  { path: 'login', component: LoginComponent },
  { path: 'contacts', component: ContactsComponent },
  { path: 'technic/home/:nif', component: TechnicHomeComponent, canActivate: [AuthGuard] },
  { path: 'order', component: OrderComponent },
  { path: 'technic/users', component: TechnicUsersComponent, canActivate: [AuthGuard] },
  { path: 'technic/users/edit/:id', component: TechnicUsersComponent, canActivate: [AuthGuard] },
  { path: 'technic/orders/edit/:id', component: TechnicOrdersComponent, canActivate: [AuthGuard] },
  { path: 'technic/orders', component: TechnicOrdersComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }

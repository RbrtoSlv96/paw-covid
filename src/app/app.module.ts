import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserDetailComponent } from '../app/components/user-detail/user-detail.component';
import { OrderDetailComponent } from '../app/components/order-detail/order-detail.component';
import { NavComponent } from '../app/components/nav/nav.component';
import { HomepageComponent } from '../app/components/homepage/homepage.component';
import { LoginComponent } from '../app/components/login/login.component';
import { ContactsComponent } from '../app/components/contacts/contacts.component';
import { NavTechnicComponent } from '../app/components/nav-technic/nav-technic.component';
import { SearchUserComponent } from './components/search-user/search-user.component';
import { UserComponent } from './components/user/user.component';
import { TechnicHomeComponent } from './components/technic-home/technic-home.component';
import { TechnicUsersComponent } from './components/technic-users/technic-users.component';
import { TechnicOrdersComponent } from './components/technic-orders/technic-orders.component';
import { OrderComponent } from './components/order/order.component';

//Material Imports
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSliderModule } from '@angular/material/slider';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatListModule } from '@angular/material/list';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { JwtInterceptor } from './components/JwInterceptor';
import { MatMenuModule } from '@angular/material/menu';
import {MatRadioModule} from '@angular/material/radio';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { OrderEditComponent } from './components/order-edit/order-edit.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';

@NgModule({
  declarations: [
    AppComponent,
    UserDetailComponent,
    OrderDetailComponent,
    NavComponent,
    HomepageComponent,
    LoginComponent,
    ContactsComponent,
    NavTechnicComponent,
    SearchUserComponent,
    UserComponent,
    TechnicHomeComponent,
    TechnicUsersComponent,
    TechnicOrdersComponent,
    OrderComponent,
    OrderEditComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatPaginatorModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatCardModule,
    MatFormFieldModule,
    MatSliderModule,
    MatToolbarModule,
    MatSlideToggleModule,
    MatButtonToggleModule,
    MatSelectModule,
    MatGridListModule,
    MatExpansionModule,
    MatListModule,
    MatRadioModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRippleModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
